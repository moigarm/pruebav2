package gui;

import javax.swing.JOptionPane;

/**
 *
 * @author Jadpa26
 */
public class Longitud extends javax.swing.JInternalFrame {

    //mi km m cm
    private double Mi_km(double mi){
        return mi*1.60934;
    }
    
    private double Km_mi(double km){
        return km/1.60934;
    }
    
    private double Km_m(double km){
        return km*1000;
    }
    
    private double M_km(double m){
        return m/1000;
    }
    
    private double M_cm(double m){
        return m*100;
    }
    
    private double Cm_m(double cm){
        return cm/100;
    }
    
    private double redondear(double temp){
        return ((Math.round(temp*100.0)/100.0));
    }
    
    /**
     * Creates new form Longitud
     */
    public Longitud() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel1 = new javax.swing.JPanel();
        cmbSelector = new javax.swing.JComboBox<>();
        spinner = new javax.swing.JSpinner();
        txtMi = new javax.swing.JTextField();
        txtKm = new javax.swing.JTextField();
        txtM = new javax.swing.JTextField();
        txtCm = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Longitud");

        jPanel1.setLayout(new java.awt.GridBagLayout());

        cmbSelector.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Milla", "Kilómetro", "Metro", "Centímetro" }));
        cmbSelector.setSelectedIndex(-1);
        cmbSelector.setToolTipText("Unidades de longitud(algunas)");
        cmbSelector.setName("cmbMain"); // NOI18N
        cmbSelector.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbSelectorItemStateChanged(evt);
            }
        });
        jPanel1.add(cmbSelector, new java.awt.GridBagConstraints());

        spinner.setToolTipText("Ingrese la cantidad a convertir");
        spinner.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spinnerStateChanged(evt);
            }
        });
        spinner.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                spinnerKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                spinnerKeyTyped(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel1.add(spinner, gridBagConstraints);

        txtMi.setToolTipText("Unidad \"Mi\"");
        txtMi.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtMiFocusGained(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(3, 8, 3, 8);
        jPanel1.add(txtMi, gridBagConstraints);

        txtKm.setToolTipText("Unidad \"Km\"");
        txtKm.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtKmFocusGained(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(3, 8, 3, 8);
        jPanel1.add(txtKm, gridBagConstraints);

        txtM.setToolTipText("Unidad \"Mt\"");
        txtM.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtMFocusGained(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(3, 8, 3, 8);
        jPanel1.add(txtM, gridBagConstraints);

        txtCm.setToolTipText("Unidad \"Cm\"");
        txtCm.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtCmFocusGained(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(3, 8, 3, 8);
        jPanel1.add(txtCm, gridBagConstraints);

        jLabel1.setText("Milla:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.weightx = 0.1;
        jPanel1.add(jLabel1, gridBagConstraints);

        jLabel2.setText("Kilómetro:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 7, 0, 17);
        jPanel1.add(jLabel2, gridBagConstraints);

        jLabel3.setText("Metro:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 20, 0, 26);
        jPanel1.add(jLabel3, gridBagConstraints);

        jLabel4.setText("Centímetro:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 8);
        jPanel1.add(jLabel4, gridBagConstraints);

        getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cmbSelectorItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbSelectorItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbSelectorItemStateChanged

    private void spinnerStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spinnerStateChanged
        // TODO add your handling code here:

        double va = (int)spinner.getValue();
        switch (cmbSelector.getSelectedIndex()) {
            case 0:
            txtMi.setText(""+redondear(va));
            txtKm.setText(""+redondear(Mi_km(va)));
            txtM.setText(""+redondear(Mi_km(Km_m(va))));
            txtCm.setText(""+redondear(Mi_km(Km_m(M_cm(va)))));
            break;
            case 1:
            txtMi.setText(""+Km_mi(va));
            txtKm.setText(""+va);
            txtM.setText(""+Km_m(va));
            txtCm.setText(""+Km_m(M_cm(va)));
            break;
            case 2:
            txtMi.setText(""+M_km(Km_mi(va)));
            txtKm.setText(""+M_km(va));
            txtM.setText(""+va);
            txtCm.setText(""+(M_cm(va)));
            break;
            case 3:
            txtMi.setText(""+Cm_m(M_km(Km_mi(va))));
            txtKm.setText(""+Cm_m(M_km(va)));
            txtM.setText(""+Cm_m(va));
            txtCm.setText(""+(M_cm(va)));
            break;
            default:
            JOptionPane.showMessageDialog(this, "Lo sentimos, su solicitud no puede ser procesada.\n"
                    + " ¡Por favor, seleccione una de las opciones!", "VAYA SUERTE", JOptionPane.ERROR_MESSAGE);
            break;
        }
    }//GEN-LAST:event_spinnerStateChanged

    private void spinnerKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_spinnerKeyReleased
        // TODO add your handling code here:
        
    }//GEN-LAST:event_spinnerKeyReleased

    private void spinnerKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_spinnerKeyTyped
        // TODO add your handling code here:
        //int CS = spinner.getComponentCount();
        

    }//GEN-LAST:event_spinnerKeyTyped

    private void txtMiFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtMiFocusGained
        // TODO add your handling code here:
        txtMi.setFocusable(false);
    }//GEN-LAST:event_txtMiFocusGained

    private void txtKmFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtKmFocusGained
        // TODO add your handling code here:
        txtKm.setFocusable(false);
    }//GEN-LAST:event_txtKmFocusGained

    private void txtMFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtMFocusGained
        // TODO add your handling code here:
        txtM.setFocusable(false);
    }//GEN-LAST:event_txtMFocusGained

    private void txtCmFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtCmFocusGained
        // TODO add your handling code here:
        txtCm.setFocusable(false);
    }//GEN-LAST:event_txtCmFocusGained


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> cmbSelector;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSpinner spinner;
    private javax.swing.JTextField txtCm;
    private javax.swing.JTextField txtKm;
    private javax.swing.JTextField txtM;
    private javax.swing.JTextField txtMi;
    // End of variables declaration//GEN-END:variables
}
