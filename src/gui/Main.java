package gui;

import javax.swing.JFrame;

/**
 *
 * @author Jadpa26
 */
public class Main extends javax.swing.JFrame {

    /**
     * Creates new form Main
     */
    public Main() {
        initComponents();
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        dsktPane = new javax.swing.JDesktopPane();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        miTemperatura = new javax.swing.JMenuItem();
        miLongitud = new javax.swing.JMenuItem();
        miPeso = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenu3 = new javax.swing.JMenu();
        miLinealDep = new javax.swing.JMenuItem();
        miSDAAscDep = new javax.swing.JMenuItem();
        miSDADescDep = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.GroupLayout dsktPaneLayout = new javax.swing.GroupLayout(dsktPane);
        dsktPane.setLayout(dsktPaneLayout);
        dsktPaneLayout.setHorizontalGroup(
            dsktPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        dsktPaneLayout.setVerticalGroup(
            dsktPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 279, Short.MAX_VALUE)
        );

        getContentPane().add(dsktPane, java.awt.BorderLayout.CENTER);

        jMenu1.setText("Conversión");

        miTemperatura.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_T, java.awt.event.InputEvent.CTRL_MASK));
        miTemperatura.setText("Temperatura");
        miTemperatura.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miTemperaturaActionPerformed(evt);
            }
        });
        jMenu1.add(miTemperatura);

        miLongitud.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_L, java.awt.event.InputEvent.CTRL_MASK));
        miLongitud.setText("Longitud");
        miLongitud.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miLongitudActionPerformed(evt);
            }
        });
        jMenu1.add(miLongitud);

        miPeso.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.CTRL_MASK));
        miPeso.setText("Peso");
        miPeso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miPesoActionPerformed(evt);
            }
        });
        jMenu1.add(miPeso);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Contabilidad");

        jMenu3.setText("Depreciación");

        miLinealDep.setText("Lineal");
        miLinealDep.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miLinealDepActionPerformed(evt);
            }
        });
        jMenu3.add(miLinealDep);

        miSDAAscDep.setText("SDA asc");
        miSDAAscDep.setToolTipText("Suma de Dígito de los Años(ascendente)");
        miSDAAscDep.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miSDAAscDepActionPerformed(evt);
            }
        });
        jMenu3.add(miSDAAscDep);

        miSDADescDep.setText("SDA desc");
        miSDADescDep.setToolTipText("Suma de Dígito de los Años(descendente)");
        miSDADescDep.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miSDADescDepActionPerformed(evt);
            }
        });
        jMenu3.add(miSDADescDep);

        jMenu2.add(jMenu3);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void miTemperaturaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miTemperaturaActionPerformed
        // TODO add your handling code here:
        
        Temperatura pt = new Temperatura();
        dsktPane.add(pt);
        pt.setVisible(true);
        
    }//GEN-LAST:event_miTemperaturaActionPerformed

    private void miLongitudActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miLongitudActionPerformed
        // TODO add your handling code here:
        
        Longitud lg = new Longitud();
        dsktPane.add(lg);
        lg.setVisible(true);
        
    }//GEN-LAST:event_miLongitudActionPerformed

    private void miPesoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miPesoActionPerformed
        // TODO add your handling code here:
        
        Peso pe = new Peso();
        dsktPane.add(pe);
        pe.setVisible(true);
        
    }//GEN-LAST:event_miPesoActionPerformed

    private void miLinealDepActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miLinealDepActionPerformed
        // TODO add your handling code here:
        
        Depreciación dp = new Depreciación();
        dsktPane.add(dp);
        dp.setVisible(true);
        
    }//GEN-LAST:event_miLinealDepActionPerformed

    private void miSDAAscDepActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miSDAAscDepActionPerformed
        // TODO add your handling code here:
        
        Depreciación dp2 = new Depreciación();
        dsktPane.add(dp2);
        dp2.setVisible(true);
        
    }//GEN-LAST:event_miSDAAscDepActionPerformed

    private void miSDADescDepActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miSDADescDepActionPerformed
        // TODO add your handling code here:
        Depreciación dp3 = new Depreciación();
        dsktPane.add(dp3);
        dp3.setVisible(true);
    }//GEN-LAST:event_miSDADescDepActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Main().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JDesktopPane dsktPane;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem miLinealDep;
    private javax.swing.JMenuItem miLongitud;
    private javax.swing.JMenuItem miPeso;
    private javax.swing.JMenuItem miSDAAscDep;
    private javax.swing.JMenuItem miSDADescDep;
    private javax.swing.JMenuItem miTemperatura;
    // End of variables declaration//GEN-END:variables
}
