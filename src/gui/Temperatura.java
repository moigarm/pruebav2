package gui;

import javax.swing.JSlider;

/**
 *
 * @author Jadpa26
 */
public class Temperatura extends javax.swing.JInternalFrame {
   
    private double Fahrenteit_celcius(double Fahren){
      return ((Fahren - 32) * 5/9);
    }
    
    private double Fahrenteit_kelvin(double Fahren){
      return ((Fahren - 32) * 5/9 + 273.15);
    }
    
    private double Celcius_fahrenheit(double celcius){
      return (celcius * 9/5) +32;
    }
    
    private double Celcius_kelvin(double celcius){
      return (celcius + 273.15);
    }
    
    private double Kelvin_celcius(double kelvin){
      return (kelvin - 273.15);
    }
    
    private double Kelvin_fahrenheit(double kelvin){
      return (kelvin - 273.15) * 9/5 + 32;
    }
    
    private double redondear(double temp){
        return ((Math.round(temp*100.0)/100.0));
    }
    
private void txtValue(){
    //Imprime los valores en los TextField
        txtCelcius.setText(""+sliCelsius.getValue());
        txtFahrenheit.setText(""+sliFahrenheit.getValue());
        txtKelvin.setText(""+sliKelvin.getValue());
}

    public Temperatura() {
        initComponents();     
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel1 = new javax.swing.JPanel();
        sliCelsius = new javax.swing.JSlider();
        sliFahrenheit = new javax.swing.JSlider();
        sliKelvin = new javax.swing.JSlider();
        lblCelcius = new javax.swing.JLabel();
        lblFahrenheit = new javax.swing.JLabel();
        lblKelvin = new javax.swing.JLabel();
        txtCelcius = new javax.swing.JTextField();
        txtFahrenheit = new javax.swing.JTextField();
        txtKelvin = new javax.swing.JTextField();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Temperatura");

        jPanel1.setAutoscrolls(true);
        jPanel1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jPanel1.setLayout(new java.awt.GridBagLayout());

        sliCelsius.setMajorTickSpacing(10);
        sliCelsius.setMinorTickSpacing(2);
        sliCelsius.setOrientation(javax.swing.JSlider.VERTICAL);
        sliCelsius.setPaintLabels(true);
        sliCelsius.setPaintTicks(true);
        sliCelsius.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        sliCelsius.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                sliCelsiusStateChanged(evt);
            }
        });
        sliCelsius.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                sliCelsiusMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                sliCelsiusMouseReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipady = 164;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 0, 0);
        jPanel1.add(sliCelsius, gridBagConstraints);

        sliFahrenheit.setMajorTickSpacing(20);
        sliFahrenheit.setMaximum(238);
        sliFahrenheit.setMinimum(32);
        sliFahrenheit.setMinorTickSpacing(5);
        sliFahrenheit.setOrientation(javax.swing.JSlider.VERTICAL);
        sliFahrenheit.setPaintLabels(true);
        sliFahrenheit.setPaintTicks(true);
        sliFahrenheit.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        sliFahrenheit.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                sliFahrenheitStateChanged(evt);
            }
        });
        sliFahrenheit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                sliFahrenheitMouseReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipady = 164;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 0, 0);
        jPanel1.add(sliFahrenheit, gridBagConstraints);

        sliKelvin.setMajorTickSpacing(20);
        sliKelvin.setMaximum(423);
        sliKelvin.setMinimum(273);
        sliKelvin.setMinorTickSpacing(2);
        sliKelvin.setOrientation(javax.swing.JSlider.VERTICAL);
        sliKelvin.setPaintLabels(true);
        sliKelvin.setPaintTicks(true);
        sliKelvin.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        sliKelvin.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                sliKelvinStateChanged(evt);
            }
        });
        sliKelvin.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                sliKelvinMouseReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipady = 164;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 0, 0);
        jPanel1.add(sliKelvin, gridBagConstraints);

        lblCelcius.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        lblCelcius.setText("    º Celcius     ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(9, 5, 0, 0);
        jPanel1.add(lblCelcius, gridBagConstraints);

        lblFahrenheit.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        lblFahrenheit.setText("° Fahrenheit");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(9, 5, 0, 0);
        jPanel1.add(lblFahrenheit, gridBagConstraints);

        lblKelvin.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        lblKelvin.setText("   º Kelvin    ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipadx = 7;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(9, 10, 0, 0);
        jPanel1.add(lblKelvin, gridBagConstraints);

        txtCelcius.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCelciusActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 62;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 9, 0);
        jPanel1.add(txtCelcius, gridBagConstraints);

        txtFahrenheit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtFahrenheitActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 62;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 9, 0);
        jPanel1.add(txtFahrenheit, gridBagConstraints);

        txtKelvin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtKelvinActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 62;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 9, 0);
        jPanel1.add(txtKelvin, gridBagConstraints);

        getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);
        jPanel1.getAccessibleContext().setAccessibleName("");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtCelciusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCelciusActionPerformed
        // TODO add your handling code here:
        
        double celcius = Double.parseDouble(txtCelcius.getText());
        sliCelsius.setValue((int)celcius);
        double fahrenheit = Celcius_fahrenheit(celcius);
        txtFahrenheit.setText(""+redondear(fahrenheit));
        sliFahrenheit.setValue((int)fahrenheit);
        double kelvin = Celcius_kelvin(celcius);
        txtKelvin.setText(""+redondear(kelvin));
        sliKelvin.setValue((int)kelvin);
        
    }//GEN-LAST:event_txtCelciusActionPerformed

    private void txtFahrenheitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtFahrenheitActionPerformed
        // TODO add your handling code here:
        
        double fahren = Double.parseDouble(txtFahrenheit.getText());
        sliFahrenheit.setValue((int)fahren);
        double celcius = Fahrenteit_celcius(fahren);
        txtCelcius.setText(""+redondear(celcius));
        sliCelsius.setValue((int)celcius);
        double kelvin = Fahrenteit_kelvin(fahren);
        txtKelvin.setText(""+redondear(kelvin));
        sliKelvin.setValue((int)kelvin);
    }//GEN-LAST:event_txtFahrenheitActionPerformed

    private void txtKelvinActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtKelvinActionPerformed
  
        double kelvin = Double.parseDouble(txtKelvin.getText());
        sliKelvin.setValue((int)kelvin);
        double celcius = Kelvin_celcius(kelvin);
        txtCelcius.setText(""+redondear(celcius));
        sliCelsius.setValue((int)celcius);
        double fahren = Kelvin_fahrenheit(kelvin);
        txtFahrenheit.setText(""+redondear(fahren));
        sliFahrenheit.setValue((int)fahren);
        
    }//GEN-LAST:event_txtKelvinActionPerformed

    private void sliCelsiusStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_sliCelsiusStateChanged

    }//GEN-LAST:event_sliCelsiusStateChanged

    private void sliFahrenheitStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_sliFahrenheitStateChanged

    }//GEN-LAST:event_sliFahrenheitStateChanged

    private void sliKelvinStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_sliKelvinStateChanged

    }//GEN-LAST:event_sliKelvinStateChanged

    private void sliCelsiusMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sliCelsiusMousePressed
 
    }//GEN-LAST:event_sliCelsiusMousePressed

    private void sliCelsiusMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sliCelsiusMouseReleased
        sliFahrenheit.setValue((int)Celcius_fahrenheit(sliCelsius.getValue()));
        sliKelvin.setValue((int)Celcius_kelvin(sliCelsius.getValue()));
        txtValue();
    }//GEN-LAST:event_sliCelsiusMouseReleased

    private void sliFahrenheitMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sliFahrenheitMouseReleased
        sliCelsius.setValue((int)Fahrenteit_celcius(sliFahrenheit.getValue()));
        sliKelvin.setValue((int)Fahrenteit_kelvin(sliFahrenheit.getValue()));
        txtValue();
    }//GEN-LAST:event_sliFahrenheitMouseReleased

    private void sliKelvinMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sliKelvinMouseReleased
        sliCelsius.setValue((int)Kelvin_celcius(sliKelvin.getValue()));
        sliFahrenheit.setValue((int)Kelvin_fahrenheit(sliKelvin.getValue()));
        txtValue();
    }//GEN-LAST:event_sliKelvinMouseReleased


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lblCelcius;
    private javax.swing.JLabel lblFahrenheit;
    private javax.swing.JLabel lblKelvin;
    private javax.swing.JSlider sliCelsius;
    private javax.swing.JSlider sliFahrenheit;
    private javax.swing.JSlider sliKelvin;
    private javax.swing.JTextField txtCelcius;
    private javax.swing.JTextField txtFahrenheit;
    private javax.swing.JTextField txtKelvin;
    // End of variables declaration//GEN-END:variables
}
