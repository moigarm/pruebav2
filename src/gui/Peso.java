package gui;

import javax.swing.JOptionPane;

/**
 *
 * @author Jadpa26
 */
public class Peso extends javax.swing.JInternalFrame {

    private double Kg_lb(double kg){
        return kg*2.20462;
    }
    
    private double Lb_kg(double lb){
        return lb/2.20462;
    }
    
    private double Kg_gm(double kg){
        return kg*1000;
    }
    
    private double Gm_kg(double gm){
        return gm/1000;
    }
    
    private double Gm_mg(double gm){
        return gm*1000;
    }
    
    private double Mg_gm(double mg){
        return mg/1000;
    }
    
    private double redondear(double temp){
        return ((Math.round(temp*10000.0)/10000.0));
    }
    
    /**
     * Creates new form Peso
     */
    public Peso() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel1 = new javax.swing.JPanel();
        cmbSelector = new javax.swing.JComboBox<>();
        txtCatcher = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        txtKg = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtGm = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtLb = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtMg = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Peso");
        setMinimumSize(new java.awt.Dimension(250, 90));

        jPanel1.setLayout(new java.awt.GridBagLayout());

        cmbSelector.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Kilogramo", "Libra", "Gramo", "Miligramo" }));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel1.add(cmbSelector, gridBagConstraints);

        txtCatcher.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCatcherActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        jPanel1.add(txtCatcher, gridBagConstraints);

        jLabel1.setText("Kilogramo:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel1.add(jLabel1, gridBagConstraints);

        txtKg.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtKgFocusGained(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        jPanel1.add(txtKg, gridBagConstraints);

        jLabel2.setText("Gramo:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel1.add(jLabel2, gridBagConstraints);

        txtGm.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtGmFocusGained(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        jPanel1.add(txtGm, gridBagConstraints);

        jLabel3.setText("Libra:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel1.add(jLabel3, gridBagConstraints);

        txtLb.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtLbFocusGained(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        jPanel1.add(txtLb, gridBagConstraints);

        jLabel4.setText("Miligramo:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel1.add(jLabel4, gridBagConstraints);

        txtMg.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtMgFocusGained(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        jPanel1.add(txtMg, gridBagConstraints);

        jLabel5.setText("     ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 1;
        jPanel1.add(jLabel5, gridBagConstraints);

        getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtKgFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtKgFocusGained
        // TODO add your handling code here:
        txtKg.setFocusable(false);
    }//GEN-LAST:event_txtKgFocusGained

    private void txtLbFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtLbFocusGained
        // TODO add your handling code here:
        txtLb.setFocusable(false);
    }//GEN-LAST:event_txtLbFocusGained

    private void txtGmFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtGmFocusGained
        // TODO add your handling code here:
        txtGm.setFocusable(false);
    }//GEN-LAST:event_txtGmFocusGained

    private void txtMgFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtMgFocusGained
        // TODO add your handling code here:
        txtMg.setFocusable(false);
    }//GEN-LAST:event_txtMgFocusGained

    private void txtCatcherActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCatcherActionPerformed
        // TODO add your handling code here:
        double va = Double.parseDouble(txtCatcher.getText());
        switch(cmbSelector.getSelectedIndex()){
            case 0:
                txtKg.setText(""+va);
                txtLb.setText(""+redondear(Kg_lb(va)));
                txtGm.setText(""+redondear(Kg_gm(va)));
                txtMg.setText(""+redondear(Kg_gm(Gm_mg(va))));
                break;
            case 1:
                txtKg.setText(""+redondear(Lb_kg(va)));
                txtLb.setText(""+va);
                txtGm.setText(""+redondear(Lb_kg(Kg_gm(va))));
                txtMg.setText(""+redondear(Lb_kg(Kg_gm(Gm_mg(va)))));
                break;
            case 2:
                txtKg.setText(""+redondear(Gm_kg(va)));
                txtLb.setText(""+redondear(Gm_kg(Kg_lb(va))));
                txtGm.setText(""+va);
                txtMg.setText(""+redondear(Gm_mg(va)));
                break;
            case 3:
                txtKg.setText(""+redondear(Mg_gm(Gm_kg(va))));
                txtLb.setText(""+redondear(Mg_gm(Gm_kg(Kg_lb(va)))));
                txtGm.setText(""+redondear(Mg_gm(va)));
                txtMg.setText(""+va);
                break;
            default:
                JOptionPane.showMessageDialog(this, "Lo sentimos, su solicitud no puede ser procesada", "VAYA SUERTE", JOptionPane.ERROR_MESSAGE);
                break;
        }
    }//GEN-LAST:event_txtCatcherActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> cmbSelector;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField txtCatcher;
    private javax.swing.JTextField txtGm;
    private javax.swing.JTextField txtKg;
    private javax.swing.JTextField txtLb;
    private javax.swing.JTextField txtMg;
    // End of variables declaration//GEN-END:variables
}
